package com.swift.info.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.okta.appauth.android.OktaAppAuth
import net.openid.appauth.AuthorizationException
import android.support.annotation.NonNull
import android.app.Activity
import android.app.PendingIntent
import android.content.ContentValues.TAG
import sun.security.jgss.GSSUtil.login
import android.content.Intent
import android.util.Log
import sun.security.jgss.GSSUtil.login




////

class LoginActivity : Activity() {

   private var mOktaAuth: OktaAppAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mOktaAuth = OktaAppAuth.getInstance(this)

        if (mOktaAuth.isUserLoggedIn()) {
            Log.i(TAG, "User is already authenticated, proceeding to protected activity")
            startActivity(Intent(this, ProtectedActivity::class.java))
            finish()
            return
        }

        // Do any of your own setup of the Activity



        mOktaAuth!!.init(
            this,
            object : OktaAppAuth.OktaAuthListener {
               override fun onSuccess() {
                    // Handle a successful initialization (e.g. display login button)

                    val completionIntent = Intent(this, AuthorizedActivity::class.java)
                    val cancelIntent = Intent(this, LoginActivity::class.java)
                    cancelIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                    mOktaAuth.login(
                        this,
                        PendingIntent.getActivity(this, 0, completionIntent, 0),
                        PendingIntent.getActivity(this, 0, cancelIntent, 0)
                    )
                }

                override fun onTokenFailure(ex: AuthorizationException) {
                    // Handle a failed initialization
                }
            }
        )


    }
}

